<?php
namespace invoiceDetails;

class Invoice {
  /**
  *  Bill Details per Invoice object
  *
  */

  public $invoiceItemsArray = [];
  public $invoiceNumber;
  public $issueDate;
  public $dueDate;
  public $payer;
  public $status;
  public $accessCode;
  public $fileName;
  public $serviceName;
  public $grandTotal;



  public function __construct() {

  }

}
