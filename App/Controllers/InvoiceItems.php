<?php
namespace invoiceDetails;

/**
*  Bill Details per Invoice object
*
*/
class InvoiceItems {

  //public $created;
  //public $serviceArray;
  public $service;
  public $serviceTime;
  public $usrname;
  public $eqname;
  public $quantity;
  public $rate;
  public $total;
  public $note;


  public function __construct() {

  }

}
