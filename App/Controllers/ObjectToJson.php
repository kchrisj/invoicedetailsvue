<?php
namespace invoiceDetails;

class ObjectToJson implements \JsonSerializable {
    public function __construct($object) {
        $this->object = $object;
    }

    public function jsonSerialize() {
        return $this->object;
    }
}
