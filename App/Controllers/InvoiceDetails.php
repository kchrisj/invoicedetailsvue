<?php
namespace invoiceDetails;

require_once(__DIR__ .'/../../vendor/autoload.php');

include("DBConnect.php");
include("Invoice.php");
include("InvoiceItems.php");
include("ObjectToJson.php");

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$data = $_POST;

$invoiceNum = $data["invnumb"]; //"kchrisj@gmail.com";
$accessCode = $data["accessCode"]; //"100-AXI-001";
$captResponse = $data['captchaResponse'];
$secretKey = '6LdR1VoUAAAAACg9y7uo8ge-ouf6ayYp0Yv9FMTg';
$userIP = $_SERVER['REMOTE_ADDR'];
date_default_timezone_set("America/New_York");
$dateFormat = date('d/m/Y H:i:s');

//$invoiceNum = 'A00003';
//$accessCode = "100-AXI-001";


// Create the logger
$userInfo = new Logger('signin');
$dbStream = new StreamHandler(__DIR__.'/../../logs//users.log', Logger::INFO);
$userInfo->pushHandler($dbStream);

//add name, code, and date of access
$userInfo->info('User signin info', array('invoice Number' => $invoiceNum, 'Access Code' => $accessCode, 'DATE: ' => $dateFormat));

$url2 = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$captResponse";
$vresponse = file_get_contents($url2);
$vresponse = json_decode($vresponse);

if(!$vresponse->success){
  echo '2';
  die();
}else
{
  $invoice_query = "SELECT CI.number, CI.issued, CI.due, CI.payer, CI.status, CI.access_code, CI.filename FROM core_invoice AS CI, people AS P
  WHERE CI.number = ? AND CI.access_code = ?";


  if($results = $dbconnect->prepare($invoice_query)){
    $results->bind_param("ss", $invoiceNum, $accessCode);
    $results->execute();
    $results->store_result();

    /* bind result variables */
    $results->bind_result($invNumber, $issued, $due, $payer, $status, $accessCode, $filename);
  } //if

  $invoiceDetails = new Invoice();

  $rowCnt = $results->num_rows;

  if($rowCnt == 0){
    echo $rowCnt;
    die();
  }
  else {
    while($row = $results->fetch()) {
      $invoiceDetails->invoiceNumber = $invNumber;
      $invoiceDetails->issueDate = $issued;
      $invoiceDetails->dueDate = $due;
      $invoiceDetails->payer = $payer;
      $invoiceDetails->status = $status;
      $invoiceDetails->accessCode = $accessCode;
      $invoiceDetails->fileName = $filename;
    }

  }

  $grandTotal = 0.00;

  $items_query = "SELECT CII.invoice, CII.service_time, CII.quantity, CII.rate, CII.total, CII.note, CONCAT_WS(' ', CU.firstname, CU.lastname) AS usrname, CS.name, CII.service
  FROM core_invoice_item AS CII, core_services CS, core_users CU
  WHERE CU.id = CII.user and CS.id = CII.service and CII.invoice = ?
  ORDER BY CII.service, CII.service_time";

  if($itemresults = $dbconnect->prepare($items_query)){
    $itemresults->bind_param("s", $invNumber);
    $itemresults->execute();
    $itemresults->store_result();

    /* bind result variables */
    $itemresults->bind_result($invoice, $serviceTime, $quantity, $rate, $total, $note, $usrname, $eqname, $service);

    $serviceIdArray = array();
    $serviceArray = array();
    while($row = $itemresults->fetch()) {
      $invoiceItems = new InvoiceItems();
      $invoiceItems->quantity = $quantity;
      $invoiceItems->serviceTime = $serviceTime;
      $invoiceItems->rate = $rate;
      $invoiceItems->total = $total;
      $invoiceItems->note = $note;
      $invoiceItems->usrname = $usrname;
      $invoiceItems->eqname = $eqname;
      $invoiceItems->service = $service;
      $invoiceItems->dueDate = $invoiceDetails->dueDate;

      if(array_key_exists($service, $serviceIdArray)){
        array_push($serviceArray, $invoiceItems);
        $serviceIdArray[$service] = $serviceArray;
      }
      else {
        $serviceArray = array();
        array_push($serviceArray, $invoiceItems);
        $serviceIdArray[$service] = $serviceArray;
      }
      $serviceIdArray[$service] = $serviceArray;
    }
    array_push($invoiceDetails->invoiceItemsArray, $serviceIdArray);

  echo json_encode(new ObjectToJson($invoiceDetails), JSON_PRETTY_PRINT);

  } //if
} //captcha if check
