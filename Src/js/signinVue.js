let url = "http://localhost/invoice/invoiceDetailsVue/App/Controllers/InvoiceDetailsDAO.php";

Vue.component('services', {
  props: ['value'],
  template: `
  <div class="columns is-half has-text-centered box">
    <table class="table is-bordered">
      <thead>
        <tr>
          <th>Service Time</th><th>Service User</th><th>Service Name</th><th>Quantity<br />(HRS)</th>
          <th>Service Rate <br />(per hr)</th><th>Total<br /> ($)</th><th>Notes</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="items in value">
          <td>{{items.serviceTime}}</td>
          <td>{{items.usrname}}</td>
          <td>{{items.eqname}}</td>
          <td>{{items.quantity}}</td>
          <td>{{items.rate}}</td>
          <td>{{items.total}}</td>
          <td>{{items.note}}</td>
        </tr>
      </tbody>
    </table>
  </div>
  `
});

new Vue({
	el:'#app',
	data:{
		invnumb: '',
		accessCode: '',
		results: [],
    showDetails: false,
    showSignin: true
	},
	methods:{
		signin:function(){
			let loginData = new FormData();
			loginData.append("invnumb", this.invnumb);
			loginData.append("accessCode", this.accessCode);
      loginData.append("captchaResponse", grecaptcha.getResponse());

			axios.post(url, loginData)
		  .then(response => {
        console.log(response.data);
        if(response.data === 2){
          alert('Verification failed!!');
        }else if(response.data === 0) {
          grecaptcha.reset();
          alert('Invoice Number or access code is incorrect');
        }else {
          console.log(response.data.invoiceItemsArray);
          this.showSignin = false;
          this.showDetails = true;
          this.results = response.data;
        }
	 	  })
			.catch(error => {
				console.log(error);
			});
		}
  }
});
