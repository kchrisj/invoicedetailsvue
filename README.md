# invoiceDetailsVue

First part of a four part system:
1. Send current invoice via email to P.I. (Invoice Sender)
2. P.I. can view details of current invoice via a web page (invoiceDetialsVue)
3. Send overdue invoice via email to P.I. (latePay)
4. P.I. can view details of over due invoice via a web page (latePayInvoiceDetailsVue)

** Uses Bulma.io and Vue.js

This is part two
    --> View current invoice details

Main files:
1.  invoiceDetailsVue/Public/
    --> index.html
        --> Sign in splash page

2.  invoiceDetailsVue/Apps/Controllers
    --> InvoiceDetails.php
        --> Query database and captcha check

3.  invoiceDetailsVue/Src/
    --> signin.js
        --> sends email to P.I.

4.  invoiceDetailsVue/log
    --> contains logfiles

5.  InvoiceSender/config
    --> config.ini
        --> info for database
